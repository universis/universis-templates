import app from './app';
import {ExpressDataApplication} from '@themost/express';
import {DocumentConverterService} from '../modules/@universis/converter/src/service';
import {assert} from 'chai';
import path from 'path';
describe('Test DocumentConverterService', ()=> {

   it('should create instance', ()=> {
        // get data application
       const dataApplication = app.get('ExpressDataApplication');
       const service = new DocumentConverterService(dataApplication);
       assert.isObject(service);
   });
    it('should register service', ()=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get('ExpressDataApplication');
        dataApplication.useService(DocumentConverterService);
        assert.isObject(dataApplication.getService(DocumentConverterService));
        assert.instanceOf(dataApplication.getService(DocumentConverterService), DocumentConverterService);
    });
    it('should use convertFile()', async ()=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get('ExpressDataApplication');
        dataApplication.useService(DocumentConverterService);
        /**
         *
         * @type {DocumentConverterService}
         */
        const service = dataApplication.getService(DocumentConverterService);
        await service.convertFile(path.resolve(__dirname, 'files/lorem-ipsum.docx'), path.resolve(__dirname, '.tmp/lorem-ipsum.pdf'))

    });
});