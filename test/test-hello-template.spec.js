import app from './app';
import {ExpressDataApplication} from '@themost/express';
import {DocumentTemplateService, DocumentTemplateLoader, GoogleDriveTemplateLoader} from '../modules/@universis/templates/src/service';
import {assert} from 'chai';
import path from 'path';
import fs from 'fs';
import GraduatedStudent from './data/GraduatedStudent';
import {DocumentConverterService} from '../modules/@universis/converter/src/service';

describe('Test DocumentTemplateService', ()=> {

    /**
     * @type {ExpressDataContext}
     */
    let context;
    before((done)=> {
        /**
         * @type {ExpressDataApplication}
         */
        const dataApplication = app.get(ExpressDataApplication.name);
        // use service
        dataApplication.useService(DocumentTemplateService);
        // use document converter
        dataApplication.useService(DocumentConverterService);
        // create context
        context = dataApplication.createContext();
        // continue
        return done();
    });

    it('should render document', async ()=> {
        // get template service
        const templateService = context.getApplication().getService(DocumentTemplateService);
        // get template path
        const templatePath = path.resolve(__dirname, 'files/HelloTemplate.docx');
        // render template
        const buffer = await templateService.render(context, templatePath, {
            department: {
                name: 'Computer Science',
                organization: {
                    name: 'Educational Institute'
                }
            },
            message: 'Hello World'
        });
        assert.instanceOf(buffer, Buffer);
        // save file
        const outFile = path.resolve(__dirname, '.tmp/HelloTemplate.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(outFile, buffer, (err)=> {
               if (err) {
                   return reject(err);
               }
               return resolve();
            });
        });
    });

    it('should export document', async ()=> {
        // get template service
        const templateService = context.getApplication().getService(DocumentTemplateService);
        /**
         * get DocumentConverterService service
         * @type {DocumentConverterService}
         */
        const converterService = context.getApplication().getService(DocumentConverterService);
        // get template path
        const templatePath = path.resolve(__dirname, 'files/HelloTemplate.docx');
        // render template
        const buffer = await templateService.render(context, templatePath, {
            department: {
                name: 'Computer Science',
                organization: {
                    name: 'Educational Institute'
                }
            },
            message: 'Hello World'
        });
        assert.instanceOf(buffer, Buffer);
        // save file
        const docFile = path.resolve(__dirname, '.tmp/HelloTemplate.docx');
        await new Promise((resolve, reject) => {
            fs.writeFile(docFile, buffer, (err)=> {
                if (err) {
                    return reject(err);
                }
                converterService.convertFile(docFile, docFile.replace(/.docx$/ig, '.pdf')).then(()=> {
                    return resolve();
                }).catch( err => {
                    return reject(err);
                });

            });
        });
    });

});
